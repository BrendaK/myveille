<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use\App\Models\User;

class UserController extends Controller
{
    public function getAll(){
        $users =User::all();
        return Response::json($users, 200);
    }

    public function getById($id){
        $user =User::find($id);
        return Response::json($user, 200);
    }

    public function create(Request $request){

        $user =new User;

        $user = User::new($user, $request);

        return Response::json($user, 200);
  
    }

    public function update($id, Request $request){
        $user = User::find($id);
        
        $user = User::new($user, $request);

        return Response::json($user, 200);

    }

    public function delete($id){
        User::destroy($id);
        return Response::json("record deleted", 200);
    }

    public function login(Request $request){
        $users = User::where("email", $request->email)->where("password", $request->password)->get();
        return Response::json($user,200);
    }

}
