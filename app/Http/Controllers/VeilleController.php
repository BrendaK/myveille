<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use\App\Models\Veille;

class VeilleController extends Controller
{
    public function getAll(){
        $veilles =Veille::all();
        return Response::json($veilles, 200);
    }

    public function getById($id){
        $veille =Veille::find($id);
        return Response::json($veille, 200);
    }

    public function create(Request $request){

        $veille =new Veille;

        $veille = Veille::new($veille, $request);

        return Response::json($veille, 200);
  
    }

    public function update($id, Request $request){
        $veille =Veille::find($id);
        
        $veille = Veille::new($veille, $request);

        return Response::json($veille, 200);

    }

    public function delete($id){
        Veille::destroy($id);
        return Response::json("record deleted", 200);
    }
}
