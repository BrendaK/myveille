<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{
    use HasFactory;

    public static function new($userType, $request){
        $userType->name = $request->name;

        $userType->save();

        return $userType;
    }
}
