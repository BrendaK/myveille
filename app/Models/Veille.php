<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Veille extends Model
{
    use HasFactory;

    public static function new($veille, $request){
        $veille->date = $request->date;
        $veille->subject = $request->subject;
        $veille->link = $request->link;
        $veille->synthesis = $request->synthesis;
        $veille->comment = $request->comment;
        $veille->image = $request->image;
        $veille->user_type = $request->user_type;

        $veille->save();

        return $veille;
    }
}
