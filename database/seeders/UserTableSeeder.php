<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = 
        [
            [
                "lastname" => "Kong",
                "firstname" => "Brenda",
                "email" => "brendakong@gmail.com",
                "password" => "azerty",
                "user_type_id" => 1,
            ]
        ];
        
        foreach ($users AS $user):
            User::create($user);
        endforeach;
    }
}
