<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\UserType;

class UserTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userTypes = 
        [
            [
                "name" => "Administrateur",
            ],
            [
                "name" => "Visiteur",
            ]
        ];
        
        foreach ($userTypes AS $userType):
            UserType::create($userType);
        endforeach;
    }
}
