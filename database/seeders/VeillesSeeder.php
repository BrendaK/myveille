<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Veille;

class VeillesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $veilles =
        [
            [
                "1",
                "2020-11-03",
                "LG s’apprêterait à dévoiler un smartphone « enroulable »",
                "https://www.presse-citron.net/lg-sappreterait-a-devoiler-un-smartphone-enroulable/",
                "En 2019, LG fait sensation en présentant un téléviseur enroulable. D'autres marques comme Samsung avaient proposé des smartphones à écrans pliables mais encore jamais avec des écrans enroulables. LG s'apprête à révolutionner la téléphonie en proposant un smartphone à écran enroulable qui devrait arriver d'ici mars 2021. ",
                "La marque coréenne ferrait un grand pas dans l'innovation en proposant une technologie encore inexistante sur le marché de la téléphonie mobile.",
                "https://dl.airtable.com/.attachments/ea374901d5dbdc0a80a7da5b435c9dbb/1f1e1c5a/lg-smartphone.jpg",
                "0"
            ],
            [
                "2",
                "2020-11-04",
                "Involontairement, une compagnie d’assurance a divulgué des millions de données personnelles aux GAFAM",
                "https://www.presse-citron.net/involontairement-une-compagnie-dassurance-a-divulgue-des-millions-de-donnees-personnelles-aux-gafam/",
                "Une société d'assurance suédoise Folksam a annoncé avoir divulgué accidentellement des données personnelles de près d'un million de clients après de Google, Facebook et Microsoft via LinkedIn. Folksam a immédiatement demandé aux GAFAM de les supprimer et de ne pas les utiliser à des fins publicitaires. La Swedish Data Inspection, l’équivalent de la CNIL, a ouvert une enquête. ",
                "La protection des données ne doit pas être pris à la légère car en cas de faute, la société se verra dans l'obligation de payer une amende pouvant aller jusqu'à 4% de son CA.",
                "https://dl.airtable.com/.attachments/0c2d4c0f401bd9ee00932bdc2c9c959f/d826c063/hacker.jpg",
                "0"
            ],
            [
                "3",
                "2020-11-05",
                "Xiaomi a un plan pour devenir le nouveau roi de la photo (à la place de Huawei)",
                "https://www.presse-citron.net/xiaomi-a-un-plan-pour-devenir-le-nouveau-roi-de-la-photo-a-la-place-de-huawei/",
                "Xiaomi a développé un nouveau type de caméra pour les smartphones qui permettrait de capturer 300 % de lumière en plus et la clarté serait améliorée de 20 %. En revanche, certaines données ne sont pas encore connues, comme la qualité du zoom sur ce capteur. Ses haut de gamme de 2020 font déjà partie des meilleurs smartphones pour prendre des photos et des vidéos. ",
                "Le constructeur de la marque  serra amené à se placer comme étant l'un des concurrents les plus sérieux de Huawei (offrant une qualité d'image exceptionnelle). ",
                "https://dl.airtable.com/.attachments/6c2e8f44d12640e59ad1a08109315991/502a8154/Une-camra-dveloppe-par-Xiaomi-1-.jpg",
                "0"
            ],
            [
                "4",
                "2020-11-06",
                "USA : submergés par la désinformation, les géants du web tentent de réagir",
                "https://www.presse-citron.net/usa-submerges-par-la-desinformation-les-geants-du-web-tentent-de-reagir/",
                "Facebook, Twitter, et les autres réseaux sociaux modèrent comme ils le peuvent les débordements, mais ils agissent souvent avec du retard. La désinformation est souvent utilisé lors de campagnes présidentielles comme c'est le cas actuellement avec le camp Trump.",
                "Les géants devraient modérer le plus rapidement possible les postes pour évider la désinformation. Voir mettre en place un système anti-désinformation à l'aide de partenariats (entre eux).",
                "https://dl.airtable.com/.attachments/a0f80081df9665b971f3635af9b357b7/b08b079c/kayla-velasquez-6Xjl5-Xq4g4-unsplash-1-.jpg",
                "0"
            ],
            [
                "5",
                "2020-11-09",
                "La fintech attire les GAFA. L’Inde réagit par une politique antitrust",
                "https://www.presse-citron.net/la-fintech-attire-les-gafa-linde-reagit-par-une-politique-antitrust/",
                "La mesure entrera en vigueur en janvier prochain, mais chacun aura deux ans pour s’y conformer. L’Inde vient de prendre une décision forte en matière de paiements dans son pays. Scruté de près par les géants de la tech pour son marché très stratégique, il y aura d’ici deux ans une politique antitrust très forte où chaque acteur ne pourra pas représenter plus de 30 % des transactions globales sur le mois.",
                "Bonne initiative de la part de l'Inde dans la mesure où les transactions des GAFAM ne pourront excéder 30% sur le mois.",
                "https://dl.airtable.com/.attachments/ab1bc7dd538a194d4c5d1eb7897f6894/a26d7420/google-pay.jpg",
                "0"
            ],
            [
                "6",
                "2020-11-10",
                "Amazon est dans le viseur de la Commission Européenne",
                "https://www.presse-citron.net/amazon-est-dans-le-viseur-de-la-commission-europeenne/",
                "La commission européen accuse Amazone d'utiliser les données des vendeurs pour optimiser ses propres ventes.",
                "Il est important d'assurer la bonne concurrence et l'équité entre les vendeurs",
                "https://dl.airtable.com/.attachments/f8051e0c62bb767835d8694e5cbaaa02/349ba10e/Union-Europenne-Commission-Europenne-1-.jpg",
                "0"
            ],
            [
                "7",
                "2020-11-12",
                "L’interface d’Instagram change et ajoute deux nouveaux onglets",
                "https://www.presse-citron.net/linterface-dinstagram-change-et-ajoute-deux-nouveaux-onglets/",
                "Instagram nous présente une toute nouvelle interface sur laquelle on peut retrouver un menu spécial shop et son propre espace TikTok",
                "Instagram sait s'adapter au changement et aux nouveautés comme on le constate avec cette nouvel interface",
                "https://dl.airtable.com/.attachments/23a1e4abc3395b001b9773c70dd1e1f1/122c69fb/instagram-affichage-maj.jpg",
                "0"
            ],
            [
                "8",
                "2020-11-13",
                "Voici ce qu’Apple pourrait faire pour éviter les accusations d’abus de position dominante",
                "https://www.presse-citron.net/voici-ce-quapple-pourrait-faire-pour-eviter-les-accusations-dabus-de-position-dominante/",
                "Apple développerait une fonctionnalité qui vous suggère des applications tierces à télécharger lorsque vous activez un nouvel iPhone ou un nouvel iPad. Dans le but de laisser une chance à ses concurrents et de ne pas reproduire l'erreur de Google qui s'est vue pénalisé d'une amende.",
                "GAFA fait bien de faire attention car est dans le viseur de la commission européenne.",
                "https://dl.airtable.com/.attachments/f70605e2d6c6447609d369f90cade8ce/d61699f0/test-iphone-12-audio.jpg",
                "0"
            ],
            [
                "9",
                "2020-11-16",
                "Le Bitcoin est arrivé sur PayPal, secousse à venir en 2021 ?",
                "https://www.presse-citron.net/le-bitcoin-est-arrive-sur-paypal-secousse-a-venir-en-2021/",
                "PayPal vient de donner le feu vert aux échanges de crypto-monnaies sur son service. Les clients américains, les premiers concernés, peuvent désormais acheter, vendre et détenir des bitcoins, litecoin, ethereum et bitcoin cash.",
                "L'utilisation de la crypto monnaie tend à être à la tendance donc il est important de suivre son évolution... Ainsi que les mouvements des BC suite à l'évolution de la crypto monnaie.",
                "https://dl.airtable.com/.attachments/0f895ba01376ea5c2782b345e0a47dd2/be3558e2/bitcoin-cours-novembre-2020.jpg",
                "0"
            ],
            [
                "10",
                "2020-11-17",
                "Google Maps : des données en temps réels sur la fréquentation des transports en commun",
                "https://www.presse-citron.net/google-maps-des-donnees-en-temps-reels-sur-la-frequentation-des-transports-en-commun/",
                "Google Maps se dote de nouvelles fonctionnalités pour aider les utilisateurs à s’adapter durant la pandémie.",
                "Il est intéressant de voir comment Google met en place de nouvelles fonctionnalités pour permettre à ses utilisateurs d'avoir un quotidien amélioré.",
                "https://dl.airtable.com/.attachments/81fd1960e8574efdcaad5a8d2123bf29/6addb136/Google-Maps-nouvelle-fonctionnalitC3A9.jpg",
                "0"
            ],
            [
                "11",
                "2020-11-18",
                "Désinformation : les patrons de Facebook et Twitter sur le grill du Sénat",
                "https://www.presse-citron.net/desinformation-les-patrons-de-facebook-et-twitter-sur-le-grill-du-senat/",
                    "Les 2 géants ont dû faire face aux questions et remarques très critiques de la part des parlementaires de tous bords au sujet de la modération des contenus sur leurs plateformes en lien avec la dernière campagne électorale.",
                    "Il est nécessaire de modérer les contenus pour éviter la désinformation...",
                    "https://dl.airtable.com/.attachments/2bea5727cd6e8f0cc2da1b690f1b4205/f7bbf9b2/zuckerberg-lobbying.jpg",
                    "0"
            ],
            [
                    "12",
                "2020-11-19",
                "Facebook est accusé de forcer ses équipes à revenir au bureau malgré les risques sanitaires",
                "https://www.presse-citron.net/facebook-est-accuse-de-forcer-ses-equipes-a-revenir-au-bureau-malgre-les-risques-sanitaires/",
                    "Certains modérateurs de Facebook se retrouvent dans l'obligation de se déplacer au bureau en cette période de pandémie. Même si Facebook affirme favoriser le télétravail, des lettres dénonçant la situation tend à prouvé le contraire.",
                    "Il me semble inadmissible que le géant des RS ne puisse pas pratiquer le télétravail par l'ensemble des membres du personnel alors que Twitter le fait.",
                    "https://dl.airtable.com/.attachments/6376212bc788d453866cdb097c7bb01f/88e300d9/zuckerberg-facebook.jpg",
                    "0"
            ],
            [
                    "13",
                "2020-11-20",
                "Vaccin contre la Covid-19 : Facebook, Google et Twitter allient leurs forces pour lutter contre la désinformation",
                "https://www.presse-citron.net/vaccin-contre-la-covid-19-facebook-google-et-twitter-allient-leurs-forces-pour-lutter-contre-la-desinformation/",
                    "Ces plateformes font leur maximum pour ne mettre en avant que des faits vérifiés, ou provenant de sources fiables. Mais un autre événement devrait mettre à mal les réseaux sociaux dans cette bataille contre la désinformation : l’arrivée du vaccin contre la Covid-19.",
                    "Il me semble important que les plateformes puissent éradiquer le plus rapidement possible les fakes news surtout en ces temps de crise sanitaire.",
                    "https://dl.airtable.com/.attachments/64d67a6389802b8900a10c728dad994c/be5c1f7b/vaccin-covid.jpg",
                    "0"
            ],
            [
                    "14",
                "2020-11-23",
                "Instagram : bientôt, un programme pour rémunérer les médias ?",
                "https://www.presse-citron.net/instagram-bientot-un-programme-pour-remunerer-les-medias/",
                "Instagram envisage de proposer un programme qui permettra aux médias de gagner de l’argent en publiant des vidéos. Actuellement, le réseau social teste déjà un programme inspiré de celui de YouTube pour permettre à des influenceurs de monétiser des vidéos publiées sur IGTV.",
                "Il serait temps qu'Instagram monétise ses vidéos comme le fait Facebook et Youtube. ",
                "https://dl.airtable.com/.attachments/55753a584c800640a6b2c30f991145ec/2bf4de0d/instagram-darkmode.jpeg",
                "0"
            ],
            [
                "15",
                "2020-11-24",
                "Facebook lance un outil de collecte pour les personnes dans le besoin",
                "https://www.presse-citron.net/facebook-lance-un-outil-de-collecte-pour-les-personnes-dans-le-besoin/",
                "Le nouvel outil drive développé par Facebook permet aux utilisateurs de collecter de la nourriture, des vêtements et d’autres produits de première nécessité pour les personnes en difficulté.",
                "Il est dommage que l’outil ne soit disponible qu’en Amérique et en France.",
                "https://dl.airtable.com/.attachments/3443f671c2079a5cda9bb2c8c46f823d/52f345c3/Facebook-2.jpg",
                    "0"
            ],
            [
                    "16",
                "2020-11-25",
                "Electric Days : l’énergie du futur se décline en version digitale le 1er décembre",
                "https://www.presse-citron.net/electric-days-lenergie-du-futur-se-decline-en-version-digitale-le-1er-decembre/",
                "Le groupe EDF souhaite faire découvrir au grand public un aperçu des solutions énergétiques pour un futur plus durable via son site web. Une plateforme serra également mit à disposition pour ceux recherchant un stage. De plus un prix serra attribué au vainqueur des lauréats...",
                "La digitalisation de l’événement me semble approprié et pertinente au vue de la situation sanitaire actuelle. ",
                "https://dl.airtable.com/.attachments/0ee96ca364ad2f0bd164db216f769297/badec935/electricdays.png",
                    "0"
            ],
            [
                    "17",
                "2020-11-26",
                "Apple rémunère des influenceurs TikTok pour promouvoir son iPhone 12 Mini",
                "https://www.presse-citron.net/apple-remunere-des-influenceurs-tiktok-pour-promouvoir-son-iphone-12-mini/",
                "Apple a fait appels à des personnalités reconnues sur TikTok pour promouvoir son iPhone12 mini.",
                "La marque fait bien de se rendre visible sur cette plateforme de plus en plus utilisé.",
                "https://dl.airtable.com/.attachments/fd0d26f2bd412a89843484a82664e6d3/cb048399/tiktok-apple.jpg",
                    "0"
            ],
            [
                    "18",
                "2020-11-27",
                "PlayStation Plus Collection : Des joueurs bannis pour avoir débloqué les jeux sans PS5",
                "https://www.jeuxvideo.com/news/1330994/playstation-plus-collection-des-joueurs-bannis-pour-avoir-debloque-les-jeux-sans-ps5.htm",
                "Face à ce catalogue alléchant, de nombreux détenteurs de PS4 ont voulu y avoir accès sans posséder eux-mêmes une PS5. Cependant, Sony s'est rapidement rendu compte de la supercherie qui transgresse ses conditions d'utilisation. Le constructeur japonais a donc décidé de bannir purement et simplement de nombreux utilisateurs ayant eu recours à cette méthode dans certains pays d'Asie du Sud-Est dont la Malaisie",
                "Mesure surprenante de la part du constructeur",
                "https://dl.airtable.com/.attachments/c23191111a3146d22dd5736dbe14e893/6963c07f/playstation-plus-collection.jpg",
                "0"
            ],
            [
                "19",
                "2020-11-30",
                "Samsung Galaxy Note : finalement, un dernier modèle en 2021 ?",
                "https://www.presse-citron.net/samsung-galaxy-note-finalement-un-dernier-modele-en-2021/",
                "D’après une nouvelle rumeur, Samsung pourrait finalement sortir un Galaxy Note en 2021. Mais il s’agirait du dernier (source non officiel).",
                "La marque n'a pas encore officialisé la nouvelle donc considérer l'information avec prudence.",
                "https://dl.airtable.com/.attachments/313cef04dfeb85516b7cc55987347ecc/2a539753/test-samsung-galaxy-note-20-spen.jpg",
                    "0"
            ],
                            [
                    "20",
                "2020-12-01",
                "Facebook aurait déboursé un milliard de dollars pour racheter Kustomer, spécialiste en gestion de la relation client",
                "https://www.presse-citron.net/facebook-aurait-debourse-un-milliard-de-dollars-pour-racheter-kustomer-specialiste-en-gestion-de-la-relation-client/",
                "Facebook veut utiliser le CRM Kustomer afin d’aider les entreprises à gérer leurs relations client via les services de messagerie (dont Messenger et WhatsApp).",
                "Les communications entre les entreprises et les clients sont un élément essentiel du modèle économique de Facebook dans le domaine de la messagerie.",
                "https://dl.airtable.com/.attachments/316cd7a6f33c708eb890dd4e3eacbbfc/e8409ef9/Facebook-logo-oeil.jpg",
"0"
            ],
            [
                "21",
                "2020-12-02",
                "Avec son nouveau fil d’actualité, Google Maps va faire de l’ombre à Facebook",
                "https://www.presse-citron.net/avec-son-nouveau-fil-dactualite-google-maps-va-faire-de-lombre-a-facebook/",
                "Google Maps propose maintenant un fil d’actualités inspiré des réseaux sociaux qui affiche des recommandations.",
                "L’arrivée de ce fil d’actualité sur Google Maps devrait donc être une bonne nouvelle pour les établissements qui ont été affectés par le confinement, à condition de maitriser ce nouveau canal de communication.",
                "https://dl.airtable.com/.attachments/edf7e17c168935f5f69931cd8b8ae2b0/2f40fbe8/Le-fil-dactualitC3A9-de-Google-Maps-1.jpg",
                "0"
            ],
            [
                "22",
                "2020-12-03",
                "Et si Xiaomi devançait tous ses concurrents en présentant le Mi 11 ce mois de décembre ?",
                "https://www.presse-citron.net/et-si-xiaomi-devancait-tous-ses-concurrents-en-presentant-le-mi-11-ce-mois-de-decembre/",
                "D'après une rumeur, Xiaomi pourait sortir pour le mois de décembre son Mi 11 qui serait le premier smartphone équipé du processeur Snapdragon 888.",
                "Ces information ne sont pas officielle donc les considérer avec prudence.",
                "https://dl.airtable.com/.attachments/41ef134297c16baa91beba3cfc5b4aca/7ce68583/test-xiaomi-mi10t-pro-prix.jpg",
                    "0"
            ],
            [
                    "23",
                "2020-12-04",
                "Google Play Music vient de disparaître pour de bon, adieu",
                "https://www.presse-citron.net/google-play-music-vient-de-disparaitre-pour-de-bon-adieu/",
                "C'est officiel, Google play music n'existe désormais plus",
                "Se tourner vers d'autres plateformes de streaming comme Spotify, Deezer...",
                "https://dl.airtable.com/.attachments/a73abf00c4657b9a208c0e0db9328dbc/3c1ae2ac/playmusic.jpg",
                    "0"
            ],
            [
                "24",
                "2020-12-07",
                "Google demande des règles claires, sans pour autant freiner l’innovation",
                "https://www.presse-citron.net/google-demande-des-regles-claires-sans-pour-autant-freiner-linnovation/",
                "Selon Matt Brittin, il serai dommage de perturber l'innovation avec des projets de loi...",
                "Google collabore avec les autorités pour mettre en place un système qui pourrait convenir à tout le monde.",
                "https://dl.airtable.com/.attachments/0face6fe7d6cae21645bced0f28fd1f4/f26c91dd/mitchell-luo-jz4ca36oJ_M-unsplash.jpg",
                "0"
            ],
            [
                "25",
                "2020-12-08",
                "Google Messages, bientôt aussi sécurisé que WhatsApp et Telegram ?",
                "https://www.presse-citron.net/google-messages-bientot-aussi-securise-que-whatsapp-et-telegram/",
                "Google Messages se dote de nouvelles fonctionnalités afin que l’app soit en mesure de rivaliser avec les services comme WhatsApp, Messenger, ou encore Telegram.",
                "Le chiffrement, une fonctionnalité nécessaire pour rivaliser avec les autres applications de messagerie instantanée",
                "https://dl.airtable.com/.attachments/d8c03eb3584e6b48e57d7390034b294c/52eff184/Ecran-Google-Pixel-5.jpg",
                "0"
            ],
            [
                "26",
                "2020-12-09",
                "Apple menace de bannir les apps qui traquent les utilisateurs sans obtenir un consentement",
                "https://www.presse-citron.net/apple-menace-de-bannir-les-apps-qui-traquent-les-utilisateurs-sans-obtenir-un-consentement/",
                "Apple s’apprête à appliquer une nouvelle règle qui obligera les développeurs à demander le consentement de l’utilisateur avant d’accéder à l’IDFA, un identifiant qui peut être utilisé pour cibler les publicités. Ceux qui ne respecteront pas cette nouvelle règle pourront être bannis de l’App Store.",
                "Il est préférable pour l'utilisateur.",
                "https://dl.airtable.com/.attachments/10a1ac44e840bb4059ffe081ca1f1cee/f77d2a7a/iphone-12-pro-max-autonomie.jpg",
                "0"
            ],
            [
                "27",
                "2020-12-10",
                "La CNIL inflige des amendes de 100 millions € à Google et de 35 millions € à Amazon",
                "https://www.presse-citron.net/la-cnil-inflige-des-amendes-de-100-millions-e-a-google-et-de-35-millions-e-a-amazon/",
                "La CNIL sanctionne Google et Amazon à cause de leurs cookies les jugeant trop intrusif.",
                "La protection des données des utilisateurs est essentielle donc les navigateurs et autres doivent respecter les mesures en vigueur sous peine de sanction.",
                "https://dl.airtable.com/.attachments/d917a43f9344586a8ea43b19e61c718d/042285f7/CafC3A9-laptop-cookies-et-bureau-travail.jpg",
                "0"
            ],
            [
                "28",
                "2020-12-11",
                "Google prend des mesures contre les fausses informations sur les vaccins",
                "https://www.presse-citron.net/google-prend-des-mesures-contre-les-fausses-informations-sur-les-vaccins/",
                "Google a rejoint Facebook et Twitter pour créer une alliance afin de lutter contre la désinformation sur les vaccins.",
                "Débat sur une perspective de vaccin depuis le début de la pandémie donc bien modérer l'information.",
                "https://dl.airtable.com/.attachments/2996f11d4b70d4f0cb3744266e9a1b99/21449e62/google.jpg",
                "0"
            ],
            [
                    "29",
                "2020-12-14",
                "Un problème technique chez Google a perturbé ses principaux services (Gmail, YouTube…) dans le monde entier",
                "https://www.lemonde.fr/pixels/article/2020/12/14/une-panne-de-google-perturbe-ses-principaux-services-gmail-youtube-dans-le-monde-entier_6063335_4408996.html",
                "Problème du système d'authentification de 45 min par les services Google (YouTube, Gmail...)",
                "La duré n'aura pas duré longtemps et réparti sur que très faiblement sur la carte.",
                "https://dl.airtable.com/.attachments/82fe824e7de95962252e6f6f3c80bdff/01b2d395/009110c_531719280-image-3.png",
                "0"
            ],
            [
                "30",
                "2020-12-15",
                "L’application TikTok aussi sur certaines Smart TV Samsung",
                "https://www.presse-citron.net/lapplication-tiktok-aussi-sur-certaines-smart-tv-samsung/",
                "TikTok est disponible sur les Smart TV Samsung.",
                "Ce réseau social est de plus en plus présent dans notre quotidien.",
                "https://dl.airtable.com/.attachments/04bf2cb4ae35e12a2cab4824f3287560/f251a32b/TikTok-Samsung-Smart-TV-2.jpg), TikTok-Samsung-Smart-TV-1.jpg (https://dl.airtable.com/.attachments/e975e19532d2b443fc55f60e5f4cff7d/dafccbff/TikTok-Samsung-Smart-TV-1.jpg",
                "0"
            ],
            [
                "31",
                "2020-12-16",
                "Netflix débarque sur un nouveau support dès aujourd’hui",
                "https://www.presse-citron.net/netflix-debarque-sur-un-nouveau-support-des-aujourdhui/",
                "Netflix arrive sur les Amazon Echo Show 10.",
                "Leader sur son marché, Netflix se fait une place dans l'univers d'Amazone.",
                "https://dl.airtable.com/.attachments/e3f326ed3de8f8f06f9e855a42a9b4f5/704cfde6/netflix-nouveautes.jpg",
                "0"
            ],
            [
                "32",
                "2020-12-17",
                "Zoom : pas de limite de 40 minutes durant les fêtes de fin d’année",
                "https://www.presse-citron.net/zoom-pas-de-limite-de-40-minutes-durant-les-fetes-de-fin-dannee/",
                "Pendant les fêtes de fin d’année, l'offre gratuite de Zoom n’aura pas de limite, ce qui permettra aux gens d’organiser des visioconférences de plus de 40 minutes, sans avoir à payer un abonnement.",
                "Zoom n’a cessé de développer de nouvelles fonctionnalités destinées à renforcer la sécurité et la confidentialité sur son service de visioconférence. Donc super nouvelle également.",
                "https://dl.airtable.com/.attachments/9ef070c1b476b3bf46437bc78eb54256/a6c51029/Zoom.jpg",
                "0"
            ],
            [
                "33",
                "2021-01-11",
                "Apple et Hyundai pourraient rapidement signer un partenariat et présenter un véhicule d’ici 2022",
                "https://www.presse-citron.net/apple-et-hyundai-pourraient-rapidement-signer-un-partenariat-et-presenter-un-vehicule-dici-2022/",
                "D'ici 2022, une version beta d'un véhicule 100% électrique créé par Apple et Hyundai.",
                "Hâte de voir ce projet se concrétiser",
                "https://dl.airtable.com/.attachments/9e8bb753b0d1f033cfb0da089eb3feb0/ca5ec656/apple-car-concept.jpg",
                "0"
            ],
            [
                "34",
                "2021-01-12",
                "Twitter annonce la suppression de 70 000 comptes liés à QAnon",
                "https://www.presse-citron.net/twitter-annonce-la-suppression-de-70-000-comptes-lies-a-qanon/",
                "Twitter ne tolère plus les publications des sympathisants de la théorie complotisteet annonce la suppression de compte liées à QAnon.",
                "Rien de surprenant quand on sait qu'en 2020, le réseau social avait supprimé 7000 comptes liés à QAnon.",
                "https://dl.airtable.com/.attachments/174623b6abbc9d1875837e1e7ece72f1/c47364b5/twitter-site.jpg",
                "0"
            ],
            [
                "35",
                "2021-01-13",
                "Razer dévoile un masque futuriste avec ventilateur, lumières et amplificateur de voix",
                "https://www.presse-citron.net/razer-devoile-un-masque-futuriste-avec-ventilateur-lumieres-et-amplificateur-de-voix/",
                "Razer dévoile un concept de masque qui, en plus d’une protection N95, inclut un ventilateur pour optimiser la respirabilité, et un système d’amplification de la voix. Sinon, le design est transparent afin de ne pas cacher les expressions faciales et un éclairage intérieur est disponible pour que ces expressions soient visibles même dans le noir.",
                "Pourle moment il ne s'agit que d'un concept donc à voir.",
                "https://dl.airtable.com/.attachments/a248bfd9a0159754eed7b9569b9d4dd8/2e60a12f/Project-Hazel-de-Razer.jpg",
                "0"
            ],
            [
                "36",
                "2021-01-14",
                "La CNIL sanctionne le ministère de l’Intérieur pour avoir utilisé des drones lors du premier confinement",
                "https://www.presse-citron.net/la-cnil-sanctionne-le-ministere-de-linterieur-pour-avoir-utilise-des-drones-lors-du-premier-confinement/",
                "Lors du premier confinement, les forces de police ont utilisé des drones pour surveiller la population. La CNIL condamne cette pratique jugée illégale.",
                "Au départ, c'était un bonne idée de faire survoler les drones pour faire tenter de respecter le confinement mais le droit à l'image n'est pas respecté avec cette pratique.",
                "https://dl.airtable.com/.attachments/08c25b7608b4b38f5f685da9518da73f/d519663b/Drone-Nice.jpg",
                "0"
            ],
            [
                "37",
                "2021-01-15",
                "En 2020, la pandémie permet au secteur des applications mobiles une expansion impressionnante",
                "https://www.presse-citron.net/en-2020-la-pandemie-permet-au-secteur-des-applications-mobiles-une-expansion-impressionnante/",
                "La pandémie aura permit une expansion des app plus importante",
                "En temps normal ce secteur n'aura pas eut besoin pour être en hausse...",
                "https://dl.airtable.com/.attachments/a014c63b6846fa585fc733666fff42a0/1d00030a/Applications.jpg",
                "0"
            ],
            [
                "38",
                "2021-01-18",
                "Apple dispose désormais d’un prototype de smartphone pliable",
                "https://www.presse-citron.net/apple-dispose-desormais-dun-prototype-de-smartphone-pliable/",
                "À son tour, Apple pourrait présenter sa vision du smartphone pliable.",
                "Apple ne serait pas le premier à sortir un modèle pliable puisque Samsung, LG... propose déjà des smartphone pliable.",
                "https://dl.airtable.com/.attachments/849a00cb0dd6af1eeaf05da21cf19f66/480ace08/Le-logo-dApple-sur-un-bC3A2timent.jpg",
                "0"
            ],
            [
                "39",
                "2021-01-19",
                "Jedi Blue » : au-delà du contrat publicitaire, un symptôme du problème des GAFA",
                "https://www.presse-citron.net/jedi-blue-contrat-probleme-gafa/",
                "Un contrat publicitaire entre Facebook et Google donne un clair aperçu du problème des GAFA, et des comportements monopolistiques tels que l'entente concurrentiel...",
                "Il faut rappeler que l'entente concurrentiel est interdite même si le terme prend un autre nom ou autre forme, les conséquences restes les mêmes.",
                "https://dl.airtable.com/.attachments/07742b5553f6782e2767c5889d9f0b4b/aa11a925/google-facebook-jedi-blue.jpg",
                "0"
            ],
            [
                "40",
                "2021-01-20",
                "Zoom sur MeWe, le réseau social « anti-Facebook » en plein boom",
                "https://www.presse-citron.net/zoom-sur-mewe-le-reseau-social-anti-facebook-en-plein-boom/",
                "L’audience de la plateforme est en très forte augmentation ces dernières semaines.",
                "Le réseau pourrait prochainement adopter une politique plus stricte pour éviter le même cas à l'avenir.",
                "https://dl.airtable.com/.attachments/29aa4133a462c11308e0e4c8d734e546/9ccc7764/Screenshot_2021-01-20-MeWe-The-best-chat-group-app-with-privacy-you-trust-.png",
                "0"
            ],
            [
                "41",
                "2021-01-21",
                "Taxe GAFA : l’administration Biden semble prête à trouver un accord global",
                "https://www.presse-citron.net/taxe-gafa-ladministration-biden-semble-prete-a-trouver-un-accord-global/",
                "Janet Yellen, la future secrétaire au Trésor américain, l’a laissé entendre lors d’une audition au Sénat ce mercredi.",
                "Good",
                "https://dl.airtable.com/.attachments/13ee70bb8e2e71f0f36b9df61adb8e89/097ca526/Screenshot_2020-11-23-AmericaE28094and-the-DemocratsE28094WonE28099t-Have-a-Future-If-Joe-Biden-Adopts-a-Centrist-Agenda-NewsClick.png",
                "0"
            ],
            [
                "42",
                "2021-01-22",
                "Ça chauffe pour les mineurs de Bitcoin",
                "https://www.presse-citron.net/ca-chauffe-pour-les-mineurs-de-bitcoin/",
                "Coopératives et particuliers, un brin bricoleurs, initient une réflexion de taille sur la consommation électrique. Avec la popularité de Bitcoin, leurs anecdotes devraient être prises au sérieux.",
                "la cryptographie à l’énergie solaire ou à d’autres énergies renouvelables, puis utiliser la chaleur résiduelle à des fins productives, est le meilleur moyen de s’engager sur une voie plus verte pour l’industrie dans son ensemble ",
                "https://dl.airtable.com/.attachments/a03f67c8f9c82789ff0483a6c34950a5/46044c3f/bitcoin-minage-chauffage-1024x682.jpg",
                "0"
            ],
            [
                "43",
                "2021-01-25",
                "Pourquoi la France ne peut pas “rater le virage” des technologies quantiques",
                "https://www.presse-citron.net/pourquoi-la-france-ne-peut-pas-rater-le-virage-des-technologies-quantiques/",
                "1,8 milliard d’euros vont être injectés ces cinq prochaines années, pour faire passer la recherche en technologies quantiques de 60 à 200 millions € par an.",
                "Il était temps",
                "https://dl.airtable.com/.attachments/cd3ffff3268eee32c19e0a207a37a224/d91ee260/plan-quantique-france.jpg",
                "0"
            ],
            [
                "44",
                "2021-01-26",
                "Test Galaxy S21 Ultra : Samsung a trouvé sa formule magique",
                "https://www.presse-citron.net/test-samsung-galaxy-s21-ultra/",
                "Annoncé mi-janvier 2021, le Galaxy S21 Ultra est le plus premium des derniers smartphones haut de gamme de Samsung. ",
                "Un super smartphone à un prix raisonnable.",
                "https://dl.airtable.com/.attachments/49650d99ea5b514ce1d8363d798d0aa1/035fcc21/test-galaxy-s21-ultra-prix.jpg",
                "0"
            ],
            [
                    "45",
                "2021-01-27",
                "Shorts : la réponse de YouTube à TikTok cartonne en bêta",
                "https://www.generation-nt.com/youtube-shorts-tiktok-video-format-court-smartphone-actualite-1984643.html",
                "YouTube Sort est un test beta qui a rencontré un grand succès en Inde avec plus de 3 milliards de vues par jour.",
                "Si une version finale est lancé? saura t-elle se faire une place face à TikTok ?",
                "https://dl.airtable.com/.attachments/9626f4265ca335b583d34530e1826350/b4fc164d/youtube-smartphone_0096006401668202.jpg",
                    "0"
            ],
            [
                    "46",
                "2021-01-28",
                "L’administration Biden souhaite reconsidérer les sanctions contre Huawei",
                "https://www.presse-citron.net/ladministration-biden-souhaite-reconsiderer-les-sanctions-contre-huawei/",
                "Bien que la position de l’administration Biden vis-à-vis de la Chine est la même que celle de l’ancienne administration Trump, la Maison Blanche envisage de réexaminer la politique américaine concernant Huawei.",
                "Affaire à suivre...",
                "https://dl.airtable.com/.attachments/4d041a13327d8d16b27dd218173f9051/d32ded68/test-huawei-mate-40-pro-ecran.jpg",
                "0"
            ],
            [
                "47",
                "2021-01-29",
                "Mark Zuckerberg ne veut pas que la politique gâche votre expérience sur Facebook",
                "https://www.clubic.com/pro/blog-forum-reseaux-sociaux/actualite-351700-sr-zuckerberg-ne-veut-pas-que-la-politique-ne-gache-votre-experience-sur-facebook.html",
                "Depuis les élections présidentielles américaines de 2016, la politique et la place qu'elle prend sur Facebook est une grosse épine dans le pied de la plateforme. Après de multiples tentatives pour faire preuve de transparence et redorer son blason, l'entreprise pense désormais à réduire la place de la politique sur le fil d'actualité des utilisateurs.",
                "Est-ce que la démarche serra vraiment efficace sachant que les internautes ont tendances de suivre les pages qui les interressent.",
                "https://dl.airtable.com/.attachments/68679df1c3d35874b6eb9d23bcda4cbe/86480f92/rawfitmaxwidth1200hash50040a249de70c803d8966c6ba3408c3a87375e8",
                    "0"
            ],
            [
                    "48",
                "2021-02-01",
                "Sur liste noire du département de la Défense des États-Unis, Xiaomi riposte avec une action devant les tribunaux américains.",
                "https://www.generation-nt.com/xiaomi-justice-plainte-usa-liste-noire-actualite-1984790.html",
                "Le fabricant chinois Xiaomi a déposé une plainte en justice qui vise les départements de la Défense et du Trésor des États-Unis. Elle fait suite à la décision de l'administration américaine d'inscrire Xiaomi sur une liste noire. Une initiative officialisée le 14 janvier, alors que l'administration Trump était encore en place.",
                "Il fallait s'attendre à une riposte de la marque, à voir ou cela va aboutir.",
                "https://dl.airtable.com/.attachments/37185a2ba8463d66e887899bf5e1e126/2352b41f/xiaomi_04B0000001672047.jpg",
                "0"
            ],
            [
                "49",
                "2021-02-02",
                "Google doit verser 3,8 millions de dollars après des accusations de discriminations raciales et sexistes",
                "https://www.presse-citron.net/google-doit-verser-38-millions-de-dollars-apres-des-accusations-de-discriminations-raciales-et-sexistes/",
                "Le département américain du Travail a tranché, Google doit payer.",
                "De plus en plus, les voix s’élèvent pour dénoncer cette mentalité sexiste et discriminatoire qui gangrène le secteur de la tech encore beaucoup trop dominé par les hommes.",
                "https://dl.airtable.com/.attachments/9e317a3f65e2ab4e421efa665641b71a/5aa48b3f/mitchell-luo-jz4ca36oJ_M-unsplash.jpg",
                "0"
            ],
            [
                "50",
                "2021-02-03",
                "Apple vise 100 000 Apple Car par an et investirait 3 milliards d'euros dans Kia pour y arriver",
                "https://www.clubic.com/pro/entreprises/apple/actualite-352295-apple-vise-100-000-apple-car-par-an-et-investirait-3-6-milliards-dans-kia-pour-y-arriver.html",
                "L'Apple Car pourrait être produite par Kia dans des usines situées aux États-Unis. Un volume de 100 000 voitures électriques par an est évoqué.",
                "Il est prévu Apple soit en charge des technologies matérielles et logicielles liées à l'aide à la conduite ou à la conduite autonome, des semiconducteurs, de la batterie...",
                "https://dl.airtable.com/.attachments/476fe6bb5cfeac1cc94a062b8ba34784/d963cd41/raw",
                "0"
            ],
            [
                "51",
                "2021-02-04",
                "Ventes sur internet : plus de 110 milliards d'euros en France en 2020",
                "https://www.generation-nt.com/ventes-ligne-fevad-france-ecommerce-crise-sanitaire-actualite-1984983.html",
                "Avec notamment l'accélération de la numérisation des magasins physiques, l'e-commerce a progressé de 8,5 % en 2020. Il compte désormais pour 13,4 % du commerce de détail.",
                "Cette évolution à la hausse du montant moyen d'une transaction est à mettre en rapport avec la situation de crise sanitaire. Il n'avait en effet pas cessé de baisser précédemment, jusqu'à atteindre son plus bas niveau historique en 2019 à 59 €.",
                "https://dl.airtable.com/.attachments/431ac9099a93285fd2da24e77ec39520/49c2747f/ecommerce_04B0000001662760.jpg",
                "0"
            ],
            [
                "52",
                "2021-02-05",
                "YouTube ajoute une nouvelle section dédiée au sport",
                "https://www.clubic.com/pro/entreprises/google/actualite-352596-youtube-ajoute-une-nouvelle-section-dediee-au-sport.html",
                "Le site d'hébergement de vidéos vient tout juste de se munir d'une catégorie inédite entièrement focalisée sur le sport. Un moyen simple et efficace d'accéder à toutes les disciplines en un clic (ou presque).",
                "YouTube Select permet aux annonceurs de cibler plus facilement les contenus populaires pour acheter de la publicité en adéquation avec les centres d'intérêt de l'audience des différentes catégories.",
                "https://dl.airtable.com/.attachments/41719648f58ed159e984c1012bce651b/e5deb671/rawfitmaxwidth1200hash088da3f2b1c0328416f1bed7919099cd6330d308",
                    "0"
            ],
            [
                    "53",
                "2021-02-08",
                "Concurrence : voici comment la Chine veut dompter ses géants de la Tech",
                "https://www.presse-citron.net/concurrence-voici-comment-la-chine-veut-dompter-ses-geants-de-la-tech/",
                "De nouvelles règles sont mises en place pour lutter contre certains abus de position dominante sur les marchés.",
                "L’Europe et les Etats-Unis s’apprêtent aussi à réguler les GAFA",
                "https://dl.airtable.com/.attachments/6e634610ae5877947d10a903e723f6a6/3497962b/chine-y-combinator.jpg",
                "0"
            ],
            [
                "54",
                "2021-02-09",
                "Facebook renforce sa lutte contre la désinformation sur la Covid-19 et les vaccins",
                "https://www.generation-nt.com/facebook-instagram-desinformation-covid-vaccin-actualite-1985099.html",
                "Facebook promet de retirer davantage de messages trompeurs et de la désinformation en rapport avec la Covid-19 et les vaccins.",
                "Facebook avait déjà annoncé de réprimer la désinformation en rapport avec la Covid et les anti-vaccins. À chaque fois, il ne parvient pas à concrétiser ces annonces.",
                "https://dl.airtable.com/.attachments/7d733b44f921dd47d2c868634b452a18/314c8002/vaccin_04B0000001671834.jpg",
                "0"
            ],
            [
                    "55",
                "2021-02-10",
                "Dopé par la soif d’actualité des internautes, Twitter continue de grandir",
                "https://www.presse-citron.net/dope-par-la-soif-dactualite-des-internautes-twitter-continue-de-grandir/",
                "Le réseau social a dépassé les 192 millions d’utilisateurs",
                "Si une version payante était proposée dans l'avenir, il faudrait resté à l'affut.",
                "https://dl.airtable.com/.attachments/bcf6d0a58d7dc818c14b10dad2a698e1/c29e62da/twitter-app.jpg",
                "0"
            ],
            [
                    "56",
                "2021-02-11",
                "Donald Trump banni à vie de Twitter",
                "https://www.generation-nt.com/donald-trump-twitter-bannissement-definitif-interdiction-actualite-1985245.html",
                "Donald Trump ne sera pas en mesure d'effectuer un retour sur Twitter, même s'il se représente à l'élection présidentielle américaine.",
                "Twitter est le premier réseau social à avoir banni l'ancien président, et envisagé de ne pas le réintégrer donc à voir si la plateforme ne revient pas sir ce qui à été dit.",
                "https://dl.airtable.com/.attachments/0c955a53731b99939588eb2651da6711/515e75d4/twitter-trump_0096006401671569.jpg",
                    "0"
            ],
            [
                    "57",
                "2021-02-12",
                "Quel est le VPN le plus rapide ?",
                "https://www.01net.com/actualites/quel-est-le-vpn-le-plus-rapide-2002021.html",
                "La nécessité de sécuriser sa connexion ou de contourner des restrictions locales sur le contenu ont fait de l’abonnement VPN un complément intéressant pour surfer de manière anonyme en masquant son adresse IP personnelle et en simulant une connexion depuis un autre pays.",
                "Pour choisir le VPN le plus rapide, il convient de faire attention à plusieurs données. Déjà il peut être intéressant de s’attarder sur les fonctionnalités les plus mises en avant par le service VPN. Certains VPN sont plus axés sur la sécurité et l’anonymat : ce sont des fonctionnalités qui peuvent ralentir potentiellement la connexion. D’autres services mettent spécifiquement en avant des serveurs rapides, optimisés pour le téléchargement et le streaming vidéo. Une autre caractéristique à surveiller est le nombre de serveurs et leur localisation. Si vous cherchez avant tout un VPN rapide pour le téléchargement, s’assurer qu’il dispose de serveurs à proximité et en grand nombre maximise vos chances d’obtenir de bons débits.",
                "https://dl.airtable.com/.attachments/fd06f242f5a90f6973e5c3f45a8ec650/babff990/8ed3a07095642fd818f9e1537cdcf.jpg",
                    "0"
            ],
            [
                    "58",
                "2021-02-15",
                "Pourquoi Mark Zuckerberg veut « faire du mal » à Apple",
                "https://www.presse-citron.net/pourquoi-mark-zuckerberg-veut-faire-du-mal-a-apple/",
                "La mise a jour IOS 14,5 va permettre aux utilisateurs d'autoriser ou non les entreprises à exploiter leurs données, soit bloquer le tracking. ",
                "Apple met la vie privée des utilisateurs au cœur de ses préoccupations, ce qui représente une bonne nouvelle pour nous.",
                "https://dl.airtable.com/.attachments/32bee3ca1e2d6ceb8c44144745cc3df4/6c699d23/mark-zuckerberg.jpg",
                "0"
            ],
            [
                "59",
                "2021-02-16",
                "Perseverance sur Mars : Avant de toucher Mars, le rover devra en passer par sept minutes de terreur",
                "https://www.20minutes.fr/sciences/2974135-20210216-conquete-spatiale-avant-toucher-mars-perseverance-devra-passer-sept-minutes-terreur",
                "Lancé en juillet dernier depuis Cap Canaveral, Mars 2020, la mission spatiale d’exploration de la planète Mars conduite par la Nasa, touchera jeudi soir au but de son voyage de 480 millions de kilomètres.
                Mais il restera encore à relever un défi de taille pour que la mission commence enfin : entrer dans l’atmosphère martienne à 20.000 km/h pour déposer en douceur le rover Perseverance, sept minutes plus tard.
                Sept minutes de terreur, décrit la Nasa. Car non seulement le vaisseau spatial devra enchaîner sans la moindre erreur une multitude d’opérations, mais on ne pourra rien faire pour l’aider depuis la Terre.",
                "La distance entre les deux planètes fait que les communications vont mettre un peu plus de onze minutes pour aller de la Terre au robot et inversement. En cas de problème, le temps que le signal nous parvienne, le robot se sera déjà écrasé.",
"https://dl.airtable.com/.attachments/b3788c4e2eafa60e26cf27019a083ad4/0e36fc9f/310x190_image-synthese-rover-perseverance-dissimule-derriere-bouclier-thermique-bien-utile-moment-traverser-atmosphere-martienne.jpg",
"0"
            ],
            [
                "60",
                "2021-02-17",
                "Les lignes de code informatique ont désormais leur sanctuaire",
                "https://www.futura-sciences.com/tech/actualites/sauvegarde-lignes-code-informatique-ont-desormais-leur-sanctuaire-84330/",
                "Pour s'assurer que la connaissance amassée par les développeurs contemporains ne se perde pas avec le temps, une entreprise américaine sauvegarde des milliers de lignes de code informatique. Un patrimoine numérique pour les générations futures confié en partie aux bibliothèques d'Alexandrie, d'Oxford et de Stanford, hauts lieux du savoir.",
                "Les logiciels sont aujourd'hui au cœur de toutes les activités humaines, de la médecine aux loisirs, des communications à l'agriculture... Il est donc légitime pour Inria de se soucier de la préservation de toute la connaissance liée au logiciel et de la mettre au service de la société, de l'industrie, de la science et de l'éducation », avait déclaré à l'époque Alexandre Petit, P.-D.G. d'Inria",
                "https://dl.airtable.com/.attachments/8709b2dd5bd08e7754f827a93e4680ad/ec25e863/c8b2752599_50170331_coffre-github.jpg",
                    "0"
            ],
            [
                    "61",
                "2021-02-18",
                "Les courses de voitures volantes électriques Airspeeder sur le point de débuter",
                "https://www.futura-sciences.com/tech/actualites/drone-courses-voitures-volantes-electriques-airspeeder-point-debuter-69525/",
                "Alauda Racing vient de dévoiler son prototype de voiture volante électrique qui se destine à concourir avec des pilotes humains dès 2022. Avant cela, des courses en mode télécommandé auront lieu cette année.",
                "Lorsque l'autonomie passera à une durée adéquat, il faudra resté à l'affut.",
                "https://dl.airtable.com/.attachments/0bc303ef7ab24ba4894ca76c43beb058/e0e9c140/2b48ad6161_50172941_untitled-design-1.jpg",
                "0"
            ],
            [
                "62",
                "2021-02-22",
                "LinkedIn pourrait créer une plateforme pour connecter les indépendants aux entreprises",
                "https://www.presse-citron.net/linkedin-pourrait-creer-une-plateforme-pour-connecter-les-independants-aux-entreprises/",
                "Le réseau social est en train de travailler sur une nouvelle plateforme qui permettrait de connecter les entreprises aux travailleurs freelance. Ce nouveau « marketplace » est censé permettre aux 740 millions d’utilisateurs du réseau social de faire appel à des travailleurs indépendants.",
                "Dès lors, LinkedIn peut vous suggérer des métiers auxquels vous n’auriez pas pensé en analysant les données de votre profil mais aussi vous suggérer des formations pour compléter celui-ci. L’entreprise citait l’exemple d’un serveur de restaurant, un métier durement affecté suite à la pandémie, qui aurait à 71 % les mêmes compétences qu’un spécialiste en service client, un métier dont la demande est toujours élevée.",
                    "https://dl.airtable.com/.attachments/f32e49f103e25c2e5cf7ab68962859d4/ad6224df/LinkedIn-sur-un-iPhone.jpg",
                    "0"
            ],
            [
                    "63",
                "2021-02-23",
                "Les pirates piratés: la Chine aurait espionné les États-Unis grâce à un outil de la NSA",
                "https://korii.slate.fr/tech/cybersecurite-chine-espionne-etats-unis-piratage-outil-nsa-epme-faille-zero-day-windows",
                "Selon l'agence de cybersécurité israélienne Check Point, la Chine se serait servie d'un outil initialement créé par la NSA américaine afin d'espionner les États-Unis. 
                Pour pouvoir exploiter cette faille et construire EpMe, la NSA avait fait le choix de la conserver secrète, plutôt que de la faire combler par ses développeurs.",

                "Un choix contestable qui lui était déjà revenu en plein visage, comme un boomerang: en 2016 et 2017, un groupe de hackers nommé Shadow Brokers avait fait fuiter les vulnérabilité logicielles secrètement exploitées par l'agence, les mettant entre toutes les mains, parfois les mauvaises, et provoquant de grands remous dans le monde de la cybersécurité.
L'affaire montre à nouveau à quel point jouer avec des vulnérabilités logicielles majeures, plutôt que de les signaler et les faire réparer, peut se révéler être un jeu dangereux, et une arme à double tranchant.",
"https://dl.airtable.com/.attachments/3a96e9d2c59622d16a6936cd407c90d8/a69086ae/clint-patterson-dyeufb8kqjk-unsplash_0.jpg",
"0"
            ],
            [
                "64",
                "2021-02-24",
                "Les informations personnelles de 500 000 patients français ont fuité sur le web",
                "https://www.presse-citron.net/les-informations-personnelles-de-500-000-patients-francais-ont-fuite-sur-le-web/",
                "En cette période de pandémie, plus que jamais, les regards sont tournés vers le secteur médical. Cela demande aux hôpitaux et autres lieux de santé de redoubler d’efforts en termes de cybersécurité. Toutefois, il semblerait que ces efforts n’ont pas été suffisants. Nous venons tout juste d’apprendre par le biais de Libération, qu’une base de données contenant les informations médicales de près de 500 000 patients circule sur le web.
                Cette base de données renferme précisément des renseignements sur 491 840 patients. On y retrouve des adresses postales, numéros de téléphone, adresses e-mail, mais également des numéros de sécurité sociale. Dans certains cas, il est également possible de connaître le groupe sanguin de la personne, le médecin traitant, ou encore la mutuelle. Des commentaires sur l’état de santé, des traitements, et autres informations concernant la séropositivité au VIH, sont aussi accessibles.",
                "Faire en sorte que le même cas ne se reproduise pas car représente une atteinte à la vie privée d'autrui. ",
                "https://dl.airtable.com/.attachments/08cab65bd2ed1c6cb094fdc6a69bd56c/35dd338f/santeCC81.jpg",
    "0"
            ],
            [
                    "65",
                "2021-02-26",
                "Que retenir du Pokémon Presents de cet après-midi ? (Legendes Pokemon Arceus, Pokemon Diamant et Perle…)",
                "https://www.presse-citron.net/que-retenir-du-pokemon-presents-de-cet-apres-midi/",
                "Le premier grand rendez-vous de cette année 2021 pour les 25 ans de la licence, ce sera le grand retour de la série “Snap” avec New Pokémon Snap le 30 avril sur Nintendo Switch. Le but sera de visiter des îles inexplorées afin d’immortaliser les Pokémon dans leur environnement naturel, à l’aide d’un appareil photo. L’objectif ultime sera bien sûr de compléter intégralement son Photodex Pokémon, avec les clichés réussis de Pokémon sauvage. Comme Nintendo le précise, « Dans New Pokémon Snap, vous explorerez des forêts, des plages et bien plus encore, pour photographier les Pokémon comme vous ne les avez jamais vus auparavant ! ».",
                "Un superbe évènement à ne surtout pas rater pour les fans de Pokemon... Il est toutefois dommage que l'évènement n'ai pas été mis en ligne en temps réel pour faire participer plus de fans.",
                "https://dl.airtable.com/.attachments/c7893264b392b220e1cc26e0186e9ffc/60cf925f/resume-pokemon-presents-26-fevrier-2021.jpg",
                    "0"
            ],
            [
                    "66",
                "2021-03-01",
                "Google avertit des millions d’utilisateurs que Google Photos peut endommager vos photos",
                "https://www.phonandroid.com/google-photos-les-images-stockees-gratuitement-seraient-de-moins-bonne-qualite-vraiment.html",
                "Google a adressé un mail aux utilisateurs de Photos les informant que la qualité de leurs images pourrait être altérée. Selon la firme, il y aurait désormais un écart significatif entre les modes de stockage Haute Qualité et Qualité Originale. Pourtant, cette dernière affirme depuis des années que la différence est à peine perceptible.",
                "D'autres alternatives existe: https://www.01net.com/astuces/cinq-alternatives-a-google-photos-pour-sauvegarder-vos-photos-et-videos-2007313.html",
                    "https://dl.airtable.com/.attachments/4bf1662ea9a13c1b35cdc7726d47b5ad/dfea78f6/google-photos.jpg",
                    "0"
            ],
            [
                    "67",
                "2021-03-02",
                "IA : les Etats-Unis veulent maintenir leur domination technologique face à la Chine",
                "https://www.01net.com/actualites/ia-les-etats-unis-veulent-maintenir-leur-domination-technologiqueface-a-la-chine-2036605.html",
                "Leaders dans le domaine de l’IA, les Etats-Unis savent que cette domination historique n’est pas définitivement acquise. Plus problématique encore, le pays de l’Oncle Sam n’a pour l’heure pas les armes pour stopper l’ascension chinoise.",
                "De l’attaque commerciale dans le domaine des semi-conducteurs (blacklistages de Huawei et de SMIC, etc.) à la prise de conscience du retard en matière d’IA, on sent que l’état américain prépare un front technologique, bien conscient de l’affrontement économique mais aussi civilisationnel avec la Chine.",
                "https://dl.airtable.com/.attachments/8aa8b8e61073063291f2d9b8ec13be72/8d076570/e002c2856069f8b47d01995b9ae34.jpg",
                "0"
            ],
            [
                "68",
                "2021-03-03",
                "TikTok crée une nouvelle instance pour améliorer sa modération en Europe",
                "https://www.presse-citron.net/tiktok-cree-une-nouvelle-instance-pour-ameliorer-sa-moderation-en-europe/",
                "L’idée est de travailler sur les politiques et les pratiques de modération. Composé de neuf membres, il comprend des représentants du monde universitaire et de la société civile du vieux continent. Le réseau social chinois entend ainsi bénéficier de l’expertise de ces derniers pour répondre aux défis auxquels il est confronté.",
                "Pour rappel TikTok doit faire face à des critiques sur sa politique de modération. En janvier dernier, une fillette italienne de dix ans est morte asphyxiée alors qu’elle participait au  « jeu du foulard » sur TikTok. Dans la foulée, l’Autorité pour la protection des données personnelles transalpine a annoncé son intention de bloquer pendant plusieurs semaines TikTok pour tous les utilisateurs qui ne sont pas en mesure de prouver qu’ils ont l’âge requis pour se connecter (13 ans).
                En février dernier, le réseau social chinois a par ailleurs été la cible d’une série de plaintes coordonnées par les agences de protection des consommateurs de l’Union européenne. Elles estiment que la plateforme a violé plusieurs règles en vigueur et font part de préoccupation spécifiques liées à la sécurité des enfants.",
                "https://dl.airtable.com/.attachments/48114cea52b91b80efa9429f56780d3f/d959e4d4/tiktok-application.jpg",
    "0"
            ],
            [
                    "71",
                "2021-03-04",
                "Phishing : Des pirates créent des mails alarmistes signés Microsoft pour dérober vos données",
                "https://www.20minutes.fr/high-tech/2989971-20210303-des-pirates-se-font-passer-pour-microsoft-pour-derober-des-informations-personnelles",
                "De nouveaux mails frauduleux prenant l’apparence des équipes de Microsoft sont actuellement en circulation. Ces derniers signalent une tentative frauduleuse de demande de récupération de compte. Pour éviter que leur compte soit piraté, les cibles sont invitées à cliquer sur un lien, afin d’annuler la demande de récupération. Il s’agit malheureusement d’un faux mail qui pourrait permettra aux auteurs de cette campagne de phishing de récupérer vos identifiants Microsoft.",
                "On ne s’en rend pas forcément compte, mais les identifiants Microsoft sont une mine d’or pour des personnes mal intentionnées. Un compte Microsoft, ce n’est pas seulement notre boîte mail, c’est aussi votre compte Xbox, Windows, Word, etc. Autrement dit, en récupérant nos identifiants Microsoft, des hackers peuvent avoir un accès à de nombreux services de la firme de Redmond et, pourquoi pas, souscrire à de nouveaux abonnements à votre insu.",
                "https://dl.airtable.com/.attachments/c8d7e270204335914c25bb056c1cf1b3/207723da/960x614_des-pirates-se-font-passer-pour-microsoft-pour-derober-des-informations-personnelles.jpg",
                    "0"
            ],
            [
                    "72",
                "2021-03-08",
                "Joe Biden recrute un conseiller partisan du démantèlement des GAFA",
                "https://www.presse-citron.net/joe-biden-recrute-un-conseiller-partisan-du-demantelement-des-gafa/",
                "Depuis l’élection de Joe Biden, beaucoup s’interrogent sur la politique qu’il entend mener sur les questions technologiques et notamment à l’égard des champions américains du secteur. L’annonce du recrutement de Tim Wu pour intégrer le Conseil économique national est très bien perçue par les progressistes et les partisans de l’application des lois antitrust.",
                "Les regards se tournent désormais sur Joe Biden qui doit annoncer prochainement les nominations à la division antitrust de la Federal Trade Commission (FTC) et au ministère de la Justice. Ces choix en diront beaucoup plus sur la détermination du dirigeant à avancer sur ces dossiers.",
                "https://dl.airtable.com/.attachments/7534cce384ad2e49e5c6efbcc2894510/5abc6e38/8088201970_99f9cdccdf_b.jpg",
                "0"
            ],
            [
                "73",
                "2021-03-09",
                "Des start-up françaises attaquent Apple pour non-respect de la législation sur la publicité ciblée",
                "https://www.01net.com/actualites/des-start-up-francaises-attaquent-apple-pour-non-respect-de-la-legislation-sur-la-publicite-ciblee-2037434.html",
                "L'organisation France Digitale, qui fédère plus de 2.000 start-up, a porté plainte mardi contre Apple auprès de la Cnil, accusant la firme de ne pas demander le consentement de ses utilisateurs pour afficher de la publicité ciblée au sein de ses propres applications. De son côté, Apple nie en bloc, dans une déclaration à l'AFP : « Les allégations de la plainte sont manifestement fausses et seront considérées pour ce qu'elles sont: une piètre tentative de ceux qui traquent les utilisateurs pour détourner l'attention de leurs propres actions et tromper les régulateurs et les décideurs politiques ». ",
                "La marque ne devrait pas accepter par défaut le consentement de l'utilisateur car va à l'encontre de la RGPD.",
                "https://dl.airtable.com/.attachments/49b6ca5dfb790f16aafb4359ec4bfb3e/9eb437e5/7ea06b0145b4a5f9ab529b715e.jpg",
                "0"
            ],
            [
                    "74",
                "2021-03-10",
                "Censurées sur Instagram, des féministes poursuivent Facebook en justice et dénoncent un deux poids, deux mesures",
                "https://fr.news.yahoo.com/censur%C3%A9es-instagram-f%C3%A9ministes-poursuivent-facebook-195449256.html",
                "L'exemple le plus flagrant et récent de la censure mise en place par Instagram, selon ces Instagrameuses et Instagrameurs qui la dénoncent, c'est une question posée en janvier dernier : Comment faire pour que les hommes arrêtent de violer ? Tous les messages et toutes les publications autour de cette question ont été supprimés. Il y a clairement deux poids, deux mesures, s'indigne Elvire Duvelle-Charles, co-créatrice du compte Clit revolution. Elle fait partie des 14 personnes qui ont assigné en référé Facebook, à qui appartient Instagram et dont les comptes comptabilisent plus d'un million d'abonnés. Au final, on se rend compte qu'Instagram peut être réactif quand il s'agit de dépublier ou de supprimer des comptes de féministes et, à contrario, on continue à avoir de toute façon de la pornographie ou de la pédopornographie sur ces réseaux-là, et qui va tenir plus longtemps.",
                "Pourquoi être plus réactif  pour dépublier des comptes féministes et à contrario être laxiste sur des comptes avec du contenu porno...?",
                "https://dl.airtable.com/.attachments/3ce0575d351aca865a09f36cf9a11049/e3a65678/54babcedd34b43a2ab03642313da445b",
                "0"
            ],
            [
                "75",
                "2021-03-11",
                "L’incendie d’un data center OVH fait état d’un bilan catastrophique",
                "https://www.presse-citron.net/lincendie-dun-data-center-ovh-fait-etat-dun-bilan-catastrophique/",
                "Il n’y a pas que les disques durs qui peuvent lâcher. L’incendie d’un data center strasbourgeois d’OVHcloud, dans la nuit du 9 au 10 mars, est venu rappeler le danger des données stockées, même sur le cloud. Ces dernières heures, le site spécialiste Netcraft a rendu un aperçu du bilan actuel des dégâts. 3,6 millions de sites seraient indisponibles. Et de nombreux ont définitivement perdu leurs données.",
                "Il n’y a pas que les disques durs qui peuvent lâcher. L’incendie d’un data center strasbourgeois d’OVHcloud, dans la nuit du 9 au 10 mars, est venu rappeler le danger des données stockées, même sur le cloud. Ces dernières heures, le site spécialiste Netcraft a rendu un aperçu du bilan actuel des dégâts. 3,6 millions de sites seraient indisponibles. Et de nombreux ont définitivement perdu leurs données.",
                "https://dl.airtable.com/.attachments/7f009dbec504051526348e4847b7f92f/9cc3b24e/Serveur-Web-Datacenter.jpg",
                "0"
            ],
            [
                "76",
                "2021-03-12",
                "Huawei P50 : le design se dévoile, et il est étonnant !",
                "https://www.presse-citron.net/huawei-p50-le-design-se-devoile-et-il-est-etonnant/",
                "En début d’année, diverses rumeurs circulaient déjà concernant les nouveaux Huawei P50 et P50 Pro. A l’instar des nouveaux iPhone ou des nouveaux Samsung Galaxy, les nouveaux flagships de chez Huawei sont scrutés de près par les férus de la marque (et les autres). Une gamme Huawei P50 qui ferait évidemment le plein de nouvelles technologies, avec en prime une section photo partiellement remaniée.",
                "En début d’année, diverses rumeurs circulaient déjà concernant les nouveaux Huawei P50 et P50 Pro. A l’instar des nouveaux iPhone ou des nouveaux Samsung Galaxy, les nouveaux flagships de chez Huawei sont scrutés de près par les férus de la marque (et les autres). Une gamme Huawei P50 qui ferait évidemment le plein de nouvelles technologies, avec en prime une section photo partiellement remaniée.",
                "https://dl.airtable.com/.attachments/c95d77b59762bb58b74aa653230d5b75/1cfbf520/Huawei-P50.jpg",
                "0"
            ],
            [
                "77",
                "2021-03-15",
                "Google risque 5 milliards de dollars pour avoir pisté les utilisateurs de Chrome en mode privé",
                "https://www.phonandroid.com/google-risque-5-milliards-de-dollars-pour-avoir-piste-les-utilisateurs-de-chrome-en-mode-prive.html",
                "Certains internautes utilisent la navigation privée en pensant être moins pistés par Google et par les sites internet visités, mais ce n’est pas le cas. En effet, le mode Incognito n’offre toujours pas une navigation privée.
                Trois utilisateurs de Google ont, en juin de l'année dernière, accusé Google de suivi excessif des données et, entre autres, qu'ils auraient dû être informés que leurs données continuaient d'être suivies même lorsque le mode de navigation privée du navigateur est utilisé. En d’autres termes, ils accusent Google d’avoir menti sur le mode Incognito et réclament plus de 5 milliards de dollars d’amende.",
                "Même si Google n’a jamais été un bon élève en matière de respect de la vie privée de ses utilisateurs, il va bientôt prendre des mesures pour supprimer progressivement les cookies sur internet, qui pistent les utilisateurs individuellement. Si Google supprime définitivement les cookies, le suivi individuel ne devrait alors plus exister sur Google d’ici 2022.",
                "https://dl.airtable.com/.attachments/98e8c770804544f222aeb3a75077f39d/44ce9655/chrome-navigation-privee-incognito.jpg",
                "0"
            ],
            [
                    "78",
                "2021-03-16",
                "Pourquoi Facebook entraîne ses intelligences artificielles avec vos vidéos",
                "https://www.01net.com/actualites/pourquoi-facebook-entraine-ses-intelligences-artificielles-avec-les-videos-de-ses-utilisateurs-2038226.html",
                "Facebook officialise le projet « Apprendre à partir de vidéos ». Il s’agit d’entraîner ses intelligences artificielles à partir de vidéos publiques postées par des utilisateurs. Des contenus visuels mais aussi audio et textuels dans des centaines de langues et de multiples pays.",
                "L’objectif ultime est de parvenir à ce que ces systèmes comprennent les vidéos aussi bien que les humains. Le site The Verge s'interroge sur les conséquences de ces expérimentations. Il craint notamment que Facebook utilise les vidéos de ses utilisateurs pour les cibler publicitairement encore plus finement. Et a priori, il ne devrait pas être possible de s’y opposer, les conditions générales d’utilisation spécifiant que l’on doit accepter que ses données soient utilisées pour la recherche et le développement de produits. ",
                "https://dl.airtable.com/.attachments/c1ff67f730711dadc9a0838cdc955554/d31f67e7/50b1df840f3ab8dc273b16e2491d8.jpeg",
                    "1"
            ],
            [
                    "79",
                "2021-03-17",
                "Piratage de Twitter : un ado de 17 ans condamné à trois ans de prison",
                "https://www.presse-citron.net/piratage-de-twitter-un-ado-de-17-ans-condamne-a-trois-ans-de-prison/",
                "Joe Biden, Barack Obama, Elon Musk, Bill Gates, Jeff Bezos, ou encore Kim Kardashian, la liste des comptes Twitter de personnalités piratés en juillet dernier est plus qu’impressionnante. Cette série de hacks a beaucoup fait parler, d’autant qu’elle était en fait une arnaque au bitcoin qui a permis de récolter 117000 dollars en quelques minutes seulement.",
                "À noter que Graham Ivan Clark n’en était pas à son coup d’essai. Il aurait également par le passé participé à des arnaques sur Minecraft et participé à un piratage visant un homme d’affaires de Seattle.",
                "https://dl.airtable.com/.attachments/291248fb6ae40a727b50e257247bb46f/4e7049c5/twitter-cyber-attaque.jpg",
                "1"
            ],
            [
                "80",
                "2021-03-18",
                "TikTok a trouvé un moyen de retenir toujours plus ses utilisateurs",
                "https://www.presse-citron.net/tiktok-a-trouve-un-moyen-de-retenir-toujours-plus-ses-utilisateurs/",
                "Voici déjà quelques années que les plateformes sociales tendent à toutes se ressembler. Tout à commencer lorsqu’Instagram a sans scrupule voler le concept du format story à Snapchat. Aujourd’hui, l’ensemble des plateformes sociales disposent d’une version des stories Snapchat. C’est également le cas pour les messages vocaux, et bien d’autres fonctionnalités que l’on retrouve sur tous les réseaux sociaux.",
                "À ce jour, bien que TikTok soit toujours interdit en Inde, le réseau social semble avoir trouvé plus de temps pour réfléchir au lancement de cette nouvelle fonctionnalité qui devrait débarquer dès les prochains mois.",
                "https://dl.airtable.com/.attachments/c9ba0d261fac0447d7bcef30a0d90c6f/1de9cff6/Space-Force-1536x864.jpg",
                    "1"
            ],
            [
                    "81",
                "2021-03-19",
                "Facebook dévoile un prototype d'interface neuronale au poignet pour (presque) contrôler la machine par la pensée",
                "https://www.20minutes.fr/high-tech/3002299-20210319-facebook-devoile-prototype-interface-neuronale-poignet-presque-controler-machine-pensee",
                "Facebook dévoile un bracelet proto d'interface neuronale pour la réalité augmentée. Ca détecte les influx nerveux (électromyographie ou EMG) pour des gestes basiques. Pour être clair, on ne parle pas d’un clavier holographique, qui demanderait de viser des touches virtuelles à la position figée. On tape sur n’importe quelle surface, et le système analyse le déplacement relatif des doigts pour deviner sur quelle touche imaginaire on a appuyé. Cela signifie que le système se calibre pour chaque individu. Facebook se croit dans la matrice et le résume ainsi : « Vous êtes le clavier. » Avec une grosse dose d’autocorrect et d’analyse prédictive, l’entreprise est persuadée qu’on finira par taper plus vite que sur un clavier physique.",
                "Facebook est l’un des leaders de la réalité virtuelle avec Oculus, et cette technologie devrait logiquement s'y inviter. Et l’entreprise devrait également lancer des lunettes connectées, en partenariat avec RayBan, cette année. Attention, ce premier modèle ne proposera pas d’affichage en réalité augmentée comme avec le casque HoloLens de Microsoft. Pour de vraies « AR glasses », la course est lancée entre Facebook et Apple, et il devrait sans doute encore falloir patienter deux ou trois ans. De quoi peaufiner ce contrôle cérébral.",
                "https://dl.airtable.com/.attachments/650b38fa7ef4fba241c17d8aa6af810c/aa204f18/310x190_facebook-devoile-prototype-bracelet-interagir-virtuel.jpg",
                    "1"
            ],
            [
                    "82",
                "2021-03-22",
                "Trump paré à faire son retour sur les réseaux sociaux, mais avec sa propre plateforme",
                "https://www.clubic.com/pro/technologie-et-politique/actualite-365770-trump-pare-a-faire-son-retour-sur-les-reseaux-sociaux-mais-avec-sa-propre-plateforme.html",
                "Presque trois mois après avoir été mis en sourdine par les principaux réseaux sociaux, dont Twitter, qui a purement et simplement bloqué son compte personnel, Donald Trump ourdit son retour… mais cette fois, avec sa propre plateforme.",
                "S'il se concrétise et que son ampleur est aussi importante que promis, le retour de Donald Trump sur une plateforme sociale pourrait conduire à une nouvelle hausse des partages de théories complotistes et d'articles de désinformation.",
                "https://dl.airtable.com/.attachments/c8bd10784b071d0ef408d3534c080fae/336cd489/rawfitmaxwidth1200hashb3562f2028b780761f7d30e041c21bafc491eecf",
                "1"
            ],
            [
                "83",
                "2021-03-23",
                "Microsoft semble s'intéresser à Discord et jusqu'à envisager un rachat à hauteur de 10 milliards de dollars.",
                "https://www.generation-nt.com/microsoft-discord-messagerie-rachat-xbox-game-pass-actualite-1986332.html",
                "Microsoft est candidat à un rachat de Discord pour plus de 10 milliards de dollars.",
                "Discord est une solution gratuite pour la plupart des utilisateurs, mais des abonnements payants Nitro sont proposés pour profiter de fonctionnalités avancées (emojis animés et personnalisés, uploads plus importants, vidéo HD et partage d'écran...).",
                "https://dl.airtable.com/.attachments/2968052e69f07da9d968d63945bda655/599cf266/01B0010401673019.jpg",
                "1"
            ],
            [
                "84",
                "2021-03-24",
                "Comment acheter des Bitcoins ? Le guide du débutant",
                "https://www.presse-citron.net/comment-acheter-bitcoin-guide-pour-un-debutant/",
                "Si vous voulez réellement acheter et détenir des Bitcoins, de l’Ether ou tout autre crypto-actif, il faudra passer par une plateforme d’échange de crypto-monnaies. En termes de volume de transactions, les leaders mondiaux sont Coinbase et Binance. Ils séduisent une clientèle de particuliers et d’investisseurs institutionnels qui assurent une grande partie des volumes.",
                "Toutefois, il existe d’autres acteurs comme ZenGo qui sont plus orientés vers le grand public et qui offrent des interfaces très intuitives. Ce dernier est connu pour offrir une application mobile ultra sécurisée qui permet d’acheter, vendre et stocker ses crypto-monnaies. Si toutes les devises numériques ne sont pas toujours présentes sur les plateformes citées, le Bitcoin est toujours disponible. Voici un début pour ceux ayant envie d'acheter des bitcoins...",
                "https://dl.airtable.com/.attachments/982c5bd2be5634cce360c3d0773565ee/180eac9c/acheter-du-bitcoin-1024x682.jpg",
                    "1"
            ],
            [
                "85",
                "2021-03-25",
                "FACEBOOK, INSTAGRAM, WHATSAPP ET MESSENGER TOUCHÉS PAR UNE PANNE",
                "https://www.bfmtv.com/tech/facebook-instagram-whats-app-et-messenger-touches-par-une-panne_AN-202103250150.html",
                "L'ensemble des services Facebook rencontrent des problèmes techniques en France ce 25 mars au matin. Pour beaucoup d'internautes français, il est impossible de se connecter aux différentes plateformes appartenant à Facebook ce 25 mars, depuis 10h30.",
                "D'après le site Downdetector, Instagram, Facebook, mais également les services de messagerie Messenger et WhatsApp sont concernés.",
                "https://dl.airtable.com/.attachments/b3a9a41fb166e344325523810cd3a7a9/49f61dcd/Lapplication-Facebook-sur-iPhone-960779.jpg",
                "1"
            ],
            [
                "86",
                "2021-03-26",
                "Certains utilisateurs pointe du doigt ces problèmes qui tend à être récurrent.",
                "https://www.clubic.com/internet/facebook/actualite-366511-facebook-met-fin-a-une-operation-de-cyberespionnage-visant-les-ouighours-hors-de-chine.html",
                "Le réseau social révèle la manière dont des hackers chinois ont utilisé sa plateforme pour partager des liens malveillants destinés à surveiller plusieurs militants ouïghours.",
                "Venant de la Chine, ce n'est pas surprenant.",
                "https://dl.airtable.com/.attachments/b3a9a41fb166e344325523810cd3a7a9/49f61dcd/Lapplication-Facebook-sur-iPhone-960779.jpg",
                "1"
            ],
        ];

        foreach ($veilles AS $tmp):
            dump($tmp);
            $veille = [
                'date' => $tmp[1],
                'subject' => $tmp[2],
                'link' => $tmp[3],
                'synthesis' => $tmp[4],
                'comment' => $tmp[5],
                'image' => $tmp[6],
                'user_type' => 1,
            ];
            Veille::create($veille);
        endforeach;
    }
}
