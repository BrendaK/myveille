<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserTypeController;
use App\Http\Controllers\VeilleController;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
|
|
|
*/

Route::get('users', [UserController::class, 'getAll']);
Route::get('users/{id}', [UserController::class, 'getById']);
Route::post('users', [UserController::class, 'create']);
Route::post('users/login', [UserController::class, 'login']);
Route::post('users/{id}', [UserController::class, 'update']);
Route::delete('users/{id}', [UserController::class, 'delete']);

Route::get('usersTypes', [UserTypeController::class, 'getAll']);
Route::get('usersTypes/{id}', [UserTypeController::class, 'getById']);
Route::post('usersTypes', [UserTypeController::class, 'create']);
Route::post('usersTypes/{id}', [UserTypeController::class, 'update']);
Route::delete('usersTypes/{id}', [UserTypeController::class, 'delete']);

Route::get('veille', [VeilleController::class, 'getAll']);
Route::get('veille/{id}', [VeilleController::class, 'getById']);
Route::post('veille', [VeilleController::class, 'create']);
Route::post('veille/{id}', [VeilleController::class, 'update']);
Route::delete('veille/{id}', [VeilleController::class, 'delete']);